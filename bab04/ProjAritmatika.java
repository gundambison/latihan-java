class ProjAritmatika{
    public static void main(String[] args){
        System.out.println("1+2+3 = "+ (1+2+3));
        System.out.println("1+2*3 = "+ (1+2*3));
        System.out.println("15 % 3 = "+ (15 % 3));
        System.out.println("13 % 5 = "+ (13 % 5));
        System.out.println("13 % 5.2 = "+ (13 % 5.2));
        System.out.println("13 / 5 = "+ (13 / 5));
        System.out.println("14 % 3 = "+ (14 % 3));
        System.out.println("15 / 5.2 = "+ (15 % 5.2));
    }
}