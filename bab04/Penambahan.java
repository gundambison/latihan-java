public class Penambahan {
    public static void main(String[] args){
        int i, j;
        i = 2;
        j = 3;
        i++;
        System.out.println("i="+i+" j="+j);

        j = 10 - i++;
        System.out.println("i="+i+" j="+j);
        
        j = 8 - ++i;
        System.out.println("i="+i+" j="+j);

        j = 16 - --i;
        System.out.println("i="+i+" j="+j);

        j = 16 - i--;
        System.out.println("i="+i+" j="+j);
    }
}
