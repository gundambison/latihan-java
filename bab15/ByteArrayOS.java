import java.io.*;

class ByteArrayOS {
    public static void main(String[] args){
        ByteArrayOutputStream keluaran = new ByteArrayOutputStream();
        String st = "Tes, Tes, ....... 12345";
        //menulis ke parameter
        for( int i=0; i < st.length(); i++){
            keluaran.write(st.charAt(i));
            //menulis ke standart output

        }

        System.out.println("Keluaran : " + keluaran );
        System.out.println("================");
        //bentuk lain
        try{
            keluaran.writeTo(System.out);
        }catch(IOException io){
            //---kosong
        }
        //-------------------
        System.out.println();
        System.out.println( "Jumlah Karakter : " + keluaran.size() );

    }
}
