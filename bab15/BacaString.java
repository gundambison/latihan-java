import java.io.*;

public class BacaString{
    public static void main(String[] args){
        int kar;
        String st = "";
        boolean selesai = false;

        System.out.println("Ketiklah kalimat dibawah ini:");
        while(!selesai){
            try{
                kar =System.in.read();
                if(kar <= -1 || kar == '\n' ){
                    selesai = true;
                }
                st = st + (char) kar;

            }catch(IOException io){
                System.err.println("ERROR: Kesalahan baca");
                selesai = true;
            }
        }

        System.out.println("String: "+ st);

    }
}