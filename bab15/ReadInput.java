import java.io.*;

class ReadInput {
    
    public static void main(String[] args){
        byte[] buffer = new byte[8]; 
//----------------------
        buffer[0]= (byte)'A'; buffer[1]= (byte)'B';
        buffer[2]= (byte)'C'; buffer[3]= (byte)'D';
        buffer[4]= (byte)'E'; buffer[5]= (byte)'F';
        buffer[6]= (byte)'G'; buffer[7]= (byte)'H';
//----------------------
        System.out.println("Masukkan 2 atau 3 huruf: ");
        try{
            System.in.read( buffer, 2, 4);
        }catch(IOException io){
            System.err.println("Kesalahan baca");
            System.exit(1);
        }

        System.out.println("Hasil: ");
        for( int i = 0; i < buffer.length; i++){
            System.out.print("Ke-" + i +": ");
            if( buffer[i] > 32 ){
                System.out.println( (char) buffer[i]);
            }else{
                System.out.println( "( " + buffer[i] + " )");
            }
        }

    }

}
