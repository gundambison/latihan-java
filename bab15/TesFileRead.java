import java.io.*;
//wajib compile dahulu

public class TesFileRead {
    public static void main(String[] args) {
        String namaBerkas;
        FileInputStream berkasMasukan = null;
        int jumByte = 0;
        byte bufferData[] = new byte[1024];
    //--------------------
        if ( args.length != 1){
            System.err.println("Penggunaan: java BacaFile namaFile");
            System.exit(1);
        }

        namaBerkas = args[0];

        try{
            berkasMasukan = new FileInputStream(namaBerkas);
        }catch(IOException io){
            System.err.println("Berkas tidak dapat dibuka");
            System.exit(1);
        }

        try{
            jumByte = berkasMasukan.available();
        }catch(IOException io){
            System.err.println("Kesalahan waktu membaca");
            System.exit(1);
        }

        System.out.println("===PROSES BACA OK==");
        //---------------
        while(jumByte > 0){
            try{
                int jumlahDibaca = berkasMasukan.read(bufferData, 0, bufferData.length);
                System.out.write( bufferData, 0, jumlahDibaca);
                jumByte = berkasMasukan.available();

            }catch(IOException i){
                System.err.println("Kesalahan I/O");
                System.exit(1);
            }
        }

        try{
            berkasMasukan.close();
        }catch(IOException io){
            //----
        }
    }   
}
