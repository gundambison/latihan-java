import java.io.*;

class BacaArray {
    public static void main(String[] args){
        int jml = 0;
        byte[] buffer = new byte[8];
//----------------------------------
        System.out.println("Ketikkan suatu kata: ");

        try{
            jml = System.in.read(buffer);
        }catch(IOException io){
            System.err.println("Kesalahan baca");
            System.exit(1);
        }

        System.out.println("Jumlah Byte: " + jml );
        System.out.println("Isi Array: ");

        //menampilkan array
        for( int i = 0; i < jml; i++ ){
            System.out.print("Karakter ke-" + i + ": ");
            if( buffer[i] > 32 )
                System.out.print( (char)buffer[i]);
            else
                System.out.print( "(" + buffer[i] + ")" );

            System.out.println("");

        }

    }
}
