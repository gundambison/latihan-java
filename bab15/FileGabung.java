import java.io.*;

class FileGabung {
    public static void main(String[] args)  throws IOException{
        //FileInputStream berkas1 = new FileInputStream("test.txt");
        //FileInputStream berkas2 = new FileInputStream("test2.txt");
        FileInputStream berkas1 =null; 
        FileInputStream berkas2 = null; 
        try{
            berkas1 = new FileInputStream("test.txt");
        }catch(IOException io){
            System.err.println("Berkas tidak dapat dibuka (1)");
            System.exit(1);
        }

        try{
            berkas2 = new FileInputStream("test2.txt");
        }catch(IOException io){
            System.err.println("Berkas tidak dapat dibuka (2)");
            System.exit(1);
        }

        SequenceInputStream masukan = new SequenceInputStream(berkas2, berkas1);
/*
 * try{
            berkasMasukan = new FileInputStream(namaBerkas);
        }catch(IOException io){
            System.err.println("Berkas tidak dapat dibuka");
            System.exit(1);
        }
 */
//-------------
        int kar;
        while( (kar = masukan.read()) != -1 ){
            System.out.print( (char) kar );
        }

        masukan.close();
        berkas2.close();
        berkas1.close();
        System.out.println();
        System.out.println("===================");
         
    }    
}
