import java.io.*;

public class ByteArrIS {
    public static void main(String[] args){
        ByteArrayOutputStream keluaran = new ByteArrayOutputStream();
        String st = "Testing, Tes, ..... 123456";

        //menulis ke keluaran
        for(int i=0; i < st.length(); i++){
            keluaran.write(st.charAt(i));
        }

        ByteArrayInputStream masukan = new ByteArrayInputStream( keluaran.toByteArray());

        int jumlahByte = masukan.available();
        byte bufferMasukan[] = new byte[jumlahByte];

        int byteDibaca  = masukan.read( bufferMasukan, 0, jumlahByte);

        //tampilkan
        for(int i=0; i < byteDibaca; i++){
            System.out.print( (char) bufferMasukan[i]);
        }

        System.out.println();

    }
}
