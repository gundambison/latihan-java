interface intLampu{
    public static final int KEADAAN_HIDUP = 1;
    public static final int KEADAAN_MATI  = 0;

    public abstract void hidupkan();
    public abstract void matikan();
}

class Lampu implements intLampu{
    private int statusLampu = 0;

    public void hidupkan(){
        if( this.statusLampu == KEADAAN_MATI){
            this.statusLampu = KEADAAN_HIDUP;
            System.out.println("Lampu Hidup");
        }else{
            System.out.println("Lampu Sudah Hidup");
        }
    }

    public void matikan(){
        if( this.statusLampu == KEADAAN_HIDUP){
            this.statusLampu = KEADAAN_MATI;
            System.out.println("Lampu mati");
        }else{
            System.out.println("Lampu Sudah mati");
        }
    }

}
//---------------------
class ProjSatu{
    public static void main(String[] args){
        Lampu objLampu = new Lampu();
        objLampu.hidupkan();
        objLampu.hidupkan();
        objLampu.matikan();
        objLampu.matikan();
    }
}