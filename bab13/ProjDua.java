interface intLampu{
    public static final int KEADAAN_HIDUP = 1;
    public static final int KEADAAN_MATI  = 0;

    public abstract void hidupkan();
    public abstract void matikan();
}
//--------------------------
interface intLampuPenyuram extends intLampu{
    public static final int POSISI_MAKSIMUM = 10;
    public static final int POSISI_MINIMUM  = 0;

    public abstract void redupkan();
    public abstract void buatLebihTerang();

}

class LampuPenyuram implements intLampuPenyuram{
    private int statusLampu = 0;

    public void hidupkan(){
        System.out.println("Hidupkan()");
        System.out.print("Lampu Hidup. ");
        this.statusLampu = POSISI_MAKSIMUM;
        System.out.println("Posisi tombol :" + this.statusLampu);
         
    }

    public void matikan(){
        System.out.println("Matikan()");
        System.out.print("Lampu Mati. ");
        this.statusLampu = POSISI_MINIMUM;
        System.out.println("Posisi tombol :" + this.statusLampu);
    }

    public void redupkan(){
        if(this.statusLampu != POSISI_MINIMUM){
            this.statusLampu --;
        }
        
        System.out.println("Redupkan()");
        if(this.statusLampu < POSISI_MINIMUM) this.statusLampu = POSISI_MINIMUM;
        if( this.statusLampu <= POSISI_MINIMUM){
            System.out.print("Lampu Mati. ");
        }else{
            System.out.print("Lampu Hidup. ");
            System.out.println("Posisi tombol :" + this.statusLampu);
        }
    }

    public void buatLebihTerang(){

        if(this.statusLampu != POSISI_MAKSIMUM ){
            this.statusLampu ++;
        }

        if(this.statusLampu > POSISI_MAKSIMUM) this.statusLampu = POSISI_MAKSIMUM;
        
        System.out.println("BuatLebihTerang()"); 
        System.out.print("Lampu Hidup. ");
        System.out.println("Posisi tombol :" + this.statusLampu); 
    }
}
//---------------------
class ProjDua{
    public static void main(String[] args){
        LampuPenyuram objLampu = new LampuPenyuram();
        objLampu.hidupkan();
        objLampu.redupkan();
        objLampu.redupkan();
        objLampu.buatLebihTerang();
        objLampu.matikan();
        objLampu.buatLebihTerang();
        objLampu.buatLebihTerang();
        objLampu.matikan();
    }

}