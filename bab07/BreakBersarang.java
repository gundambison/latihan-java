public class BreakBersarang {
    public static void main(String[] args){
        int i = 0, j = 0;
        System.out.println("While break normal");
        while( i < 3){
            j = 0;
            while(j < 5){
                if(j == 3) break;

                System.out.println("i: " + i + ", j: " + j);
                j++;
            }
            i++;
        }

        System.out.println("While break label");
        i=0; j=0;
        selesai:
        while( i < 3){
            j = 0;
            while(j < 5){
                if(j == 3) break selesai;

                System.out.println("i: " + i + ", j: " + j);
                j++;
            }
            i++;
        }
        System.out.println("(last) i: " + i + ", j: " + j);

    }
}
