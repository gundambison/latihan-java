public class ProjSegitiga {
    public static void main(String[] args){
        System.out.print("Input angka: ");
        String st = BacaKeyboard.bacaString();

        int i, j;
        i = Integer.valueOf(st).intValue();
        System.out.println("anda memasukkan : "+ i);

        for(int langkah=1;langkah < i; langkah++){
            j = i - langkah;
            
            if(langkah % 4 == 3){
                for(int langkah2=1; langkah2 <  (j*2); langkah2++){
                    System.out.print("-");
    
                }
                
                for(int langkah2=1; langkah2 <  langkah*2; langkah2++){
                    System.out.print("-");
    
                }
                System.out.println("");
                continue;
            } 

            for(int langkah2=j; langkah2 > 0; langkah2--){
                System.out.print(" ");
            }
            for(int langkah2=1; langkah2 <  (langkah*2); langkah2++){
                System.out.print("*");

            }
            System.out.println("");
            if(langkah > 10) break;
        }
    }
}
