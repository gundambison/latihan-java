import java.io.*;

public class BacaKeyboard {
 /*
  * harap di compile dan class digunakan 
  * pada bab2 berikutnya
  */
    public static String bacaString(){
        int karakter;
        String str = "";
        boolean selesai = false;
        
        while(!selesai){
            try{
                karakter = System.in.read();
                if( karakter < 0|| (char) karakter == '\n' ){
                    selesai=true;
                }else if( (char) karakter != '\r')
                {
                    str = str + (char) karakter;
                }
            }
            catch( java.io.IOException s){
                System.err.println("Ada kesalahan");
                selesai = true;
            }

        }

        return str;

    }

}