import java.util.*;

public class Date3 {
    public static void main(String[] args){
        
        GregorianCalendar sekarang = new GregorianCalendar();
        infoWaktu(sekarang);
        sekarang.add(Calendar.DATE, 5);
        infoWaktu(sekarang);
        
        sekarang.add(Calendar.MONTH, 3);
        infoWaktu(sekarang);

        sekarang.add(Calendar.MONTH, -4);
        infoWaktu(sekarang);
    }

    static void infoWaktu(GregorianCalendar cal ){
        String hari[] = {
            "-", "Minggu", "Senin", "Selasa",
            "Rabu", "Kamis", "Jum'at",
            "Sabtu"
        };
        String bulan[] ={
            "Januari", "Februari", "Maret",
            "April", "Mei", "Juni",
            "Juli", "Agustus", "September",
            "Oktober", "November", "Desember"
        };

        System.out.println("Hari: "+ hari[ cal.get(Calendar.DAY_OF_WEEK)] +
        " Tanggal: " + cal.get(Calendar.DATE) +
        " Bulan: " + bulan[ cal.get(Calendar.MONTH)] +
        " Tahun: " + cal.get(Calendar.YEAR)
        );
    }
}
