import java.util.*;
public class Date2 {
    public static void main(String[] args){
        GregorianCalendar sekarang = new GregorianCalendar();
        infoWaktu(sekarang);
        GregorianCalendar kemerdekaan = new GregorianCalendar(2023, 7, 17);
        infoWaktu(kemerdekaan);
    }

    static void infoWaktu(GregorianCalendar cal ){
        String hari[] = {
            "-", "Minggu", "Senin", "Selasa",
            "Rabu", "Kamis", "Jum'at",
            "Sabtu"
        };
        String bulan[] ={
            "Januari", "Februari", "Maret",
            "April", "Mei", "Juni",
            "Juli", "Agustus", "September",
            "Oktober", "November", "Desember"
        };

        System.out.println("Hari: "+ hari[ cal.get(Calendar.DAY_OF_WEEK)] +
        " Tanggal: " + cal.get(Calendar.DATE) +
        " Bulan: " + bulan[ cal.get(Calendar.MONTH)] +
        " Tahun: " + cal.get(Calendar.YEAR)
        );
    }
}
