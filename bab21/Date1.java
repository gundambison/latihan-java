import java.util.Calendar;

public class Date1{
    public static void main(String[] args){
        String hari[] = {
            "-", "Minggu", "Senin", "Selasa",
            "Rabu", "Kamis", "Jum'at",
            "Sabtu"
        };

        Calendar sekarang = Calendar.getInstance();

        System.out.println("Tanggal sekarang: " + sekarang.get(Calendar.DAY_OF_MONTH) +
        "/" + sekarang.get(Calendar.MONTH) +
        "/" + sekarang.get(Calendar.YEAR) +
        "|" + sekarang.get(Calendar.DAY_OF_WEEK) );

        Calendar tglKemerdekaan = Calendar.getInstance( );
        System.out.println("Hari kemerdekaan : " + hari[ tglKemerdekaan.get(Calendar.DAY_OF_WEEK) ]);
    }
} 