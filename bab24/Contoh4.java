public class Contoh4{
    
    public static void main(String[] args){
        Motor m1 = new Motor("M-1",800);
        Motor m2 = new Motor("M-2",650);

        m1.start();
        m2.start();

        boolean m1End = false;
        boolean m2End = false;

        do{
            if( !m1End && !m1.isAlive()){
                m1End = true;
                System.out.println("Thread m1 berakhir");
            }
            if( !m2End && !m2.isAlive()){
                m2End = true;
                System.out.println("Thread m2 berakhir");
            }
        }while(!m1End || !m2End);

    }

}

class Motor extends Thread{
private int sleepTime;
    public Motor(String name, int sleepTime){
        super(name);
        this.sleepTime = sleepTime;
    }

    public void run(){
        String name = getName();
        for(int i=0; i < 11; i++){
            try{
                sleep(this.sleepTime);
            }catch(InterruptedException ie){
                System.out.println("Terinterupsi");
            }
    //------
            System.out.println("Thread :" + name +"| Posisi :" + i);
        }
    }
}
