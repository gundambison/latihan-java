import java.util.Arrays;
public class Contoh3{
    public static void main(String[] args) {
        // Create a new thread and start it
        MyThread thread = new MyThread();
        thread.start();

        // Print numbers from 1 to 10
        for (int i = 1; i <= 10; i++) {
            System.out.println(i);
        }
    }
}

class MyThread extends Thread {
    public void run() {
        // Print letters from A to J
        for (char ch = 'A'; ch <= 'J'; ch++) {
            System.out.println(ch);
            try{
                sleep(10);//wait 
            }
            catch(InterruptedException ie){
                System.out.println("Terinterupsi");
            }
        }
    }
}