public class TesSinkron {
    public static void main(String[] args){
        Kapal m1 = new Kapal("M-1",500);
        Kapal m2 = new Kapal("M-2",500);

        m1.start();
        m2.start();

    }
    
}

class Kapal extends Thread{
    private int sleepTime;
        public Kapal(String name, int sleepTime){
            super(name);
            this.sleepTime = sleepTime;
        }
    
        public void run(){
            String name = getName();
            SinkronisasiKeluaran.info(name, this.sleepTime);
        }
    
}

class SinkronisasiKeluaran{
    public static synchronized void info(String nama, int sleepTime){
        for(int i=0; i < 5; i++){
            try{
                Thread.sleep( sleepTime );
            }catch(InterruptedException ie){
                System.out.println("Terinterupsi");
            }

            System.out.println("Thread :" + nama +"| pos: "+ i);
        }
    } 
}