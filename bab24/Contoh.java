import java.util.Arrays;

public class Contoh {
    public static void main(String[] args){
        Mobil m1 = new Mobil("M-1");
        Mobil m2 = new Mobil("M-2");

        m1.start();
        m2.start();
    }
}

class Mobil extends Thread{
    public Mobil(String id ){
        super(id);
    }

    public void run(){
        String nama = getName();
        for(int i=0; i < 5; i++){
            try{
                sleep(1001);//wait 1 detik
            }
            catch(InterruptedException ie){
                System.out.println("Terinterupsi");
            }

            System.out.println("Thread " + nama + ": posisi " + i);
        }

        return ;
    }
}