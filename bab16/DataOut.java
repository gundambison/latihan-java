import java.io.*;

public class DataOut{
    public static void main( String[] args) throws IOException{
        double data[] = new double[5];
        String namaBerkas ="double.dat";
        data[0] = 34.5;
        data[1] = 678.5;
        data[2] = 68.2;
        data[3] = 56.5;
        data[4] = 1251.6;
    //-----------------
        FileOutputStream berkas = new FileOutputStream(namaBerkas);
        DataOutputStream output = new DataOutputStream(berkas);
    //-----------------
        for(int i = 0; i < data.length; i++ ){
            output.writeDouble(data[i]);
        }

        System.out.println("Data telah disimpan");
        berkas.close();
//====cek baca
        FileInputStream berkasMasukan = null;
        try{
            berkasMasukan = new FileInputStream(namaBerkas);
        }catch(IOException io){
            System.err.println("Berkas tidak dapat dibuka");
            System.exit(1);
        }

        System.out.println("Berkas tersedia");
    }
}