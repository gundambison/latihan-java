import java.io.*;
//jalankan DataOutput terlebih dahulu
public class DataInput {
    public static void main( String[] args) throws IOException{
        //FileInputStream berkas = new FileInputStream("double.dat");        
        FileInputStream berkas = null;
        String namaBerkas = "double.dat";

        try{
            berkas = new FileInputStream(namaBerkas);
            System.out.println("File : " + namaBerkas + " (berhasil di load)" );

        }catch(IOException io){
            System.err.println("Berkas tidak dapat dibuka");
            System.exit(1);
        }

        DataInputStream inputs = new DataInputStream(berkas);
    //-------
        double bilangan ;
        int nomor = 0;

    //---baca data
        while( inputs.available() > 0){
            bilangan = inputs.readDouble();
            nomor++;
            System.out.println( nomor +" : " + bilangan );
        }

        berkas.close();
    }
}
