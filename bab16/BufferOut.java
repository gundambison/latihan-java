import java.io.*;

public class BufferOut {
    public static void main(String[] args) throws IOException{
        String st = "*Test 123,... tes 123";
        //----
        BufferedOutputStream streamKeluaran = new BufferedOutputStream(System.out);

        for(int i = 0; i < st.length(); i++ ){
            streamKeluaran.write(st.charAt(i));
        }
        System.out.println("Selesai ");
        //komen dibawah ini agar muncul infonya
        streamKeluaran.flush();

    }    
}
