import java.io.*;

public class BufferInput {
    public static void main(String[] args) throws IOException{
        FileInputStream berkasProgam = new FileInputStream("BufferInput.java"); //baca diri sendiri
        BufferedInputStream streamMasukan = new BufferedInputStream(berkasProgam);

        char kar;
        kar = (char) streamMasukan.read();
        System.out.print(kar);

        berkasProgam.close();

        for(int i = 0; i < 512 ; i++){
            kar = (char) streamMasukan.read();
            System.out.print(kar);
        }

        System.out.println();
        
    }
}
