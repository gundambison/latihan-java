import java.io.*;

public class InfoFile{
    public static void main(String[] args){
        BufferedReader streamText = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("masukkan nama file: ");
        String namaBerkas = null;

        try{
            namaBerkas = streamText.readLine();
        }catch(IOException io){}
    //------
        File berkas = new File(namaBerkas);
        if( !berkas.exists()){
            System.out.println("Berkas tidak ada");
            System.exit(1);
        }
    //-----
        if(berkas.isDirectory()) System.out.println("Berkas adalah direktori.");
        
        if(berkas.isFile()) System.out.println("Berkas adalah File.");
        
        if(berkas.isHidden()) System.out.println("Berkas tersembunyi.");
        
        if(berkas.canRead()) System.out.println("Berkas dapat dibaca.");

        if(berkas.canWrite()) System.out.println("Berkas dapat ditulisi.");
        
        if(berkas.canExecute()) System.out.println("Berkas dapat dijalankan.");

        if(berkas.isAbsolute()){
            System.out.println("Berkas pada path absolute.");
        }else{
            System.out.println("Berkas pada path relatif.");
        }
    //------------
        String induk = berkas.getParent();
        String path = berkas.getPath();
        String pathAbs = berkas.getAbsolutePath();
        String nama = berkas.getName();
        long ukuran = berkas.length();

        System.out.println("Induk dari    : " + induk );
        System.out.println("Path          : " + path );
        System.out.println("Path Absolute : " + pathAbs );
        System.out.println("Nama          : " + nama );
        System.out.println("Ukuran        : " + ukuran );
        System.out.println("==============="  );
        
    }
}