import java.io.*;

public class GantiNama {
    public static void main(String[] args){
        String namaFile = "nama1.tmp";
        String namaBaru = "nama2.tmp";
        try{
            FileOutputStream berkasTmp = new FileOutputStream(namaFile);
            berkasTmp.close();
            System.out.println("berkas :" + namaFile + " (dibuat)");
        }catch(IOException io){}

    //akses kelas file
        File BerkasSebelum = new File(namaFile);
        File BerkasBaru    = new File(namaBaru);

        //berkas dihapus untuk jaga-jaga
        if(BerkasBaru.exists()){
            BerkasBaru.delete();
            System.out.println("berkas :" + namaBaru + " (dihapus)");
        }
    //----ganti nama 
        BerkasSebelum.renameTo(BerkasBaru);
        System.out.println("Nama Sudah diganti");
        if(BerkasBaru.exists()){
            System.out.println("berkas :" + namaBaru + " (tersedia)");
        } 
        
    }
}
