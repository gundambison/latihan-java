import java.io.*;
public class HapusFile { 
    public static void main(String[] args){
        String namaFile = "fileBaru.tmp";
    //---buat file
        try{
            FileOutputStream berkasTmp = new FileOutputStream(namaFile);
            berkasTmp.close();
            System.out.println("berkas :" + namaFile + " (dibuat)");
        }catch(IOException io){}

    //-----akses
        File berkas = new File(namaFile);
    //---cek
        if(berkas.exists()){
            System.out.println("berkas :" + namaFile + " (tersedia)");
        } else{
            System.out.println("Berkas :" + namaFile + " (sudah dihapus)");
        }

        berkas.delete();
        System.out.println("Setelah penghapusan =====");

        if(berkas.exists()){
            System.out.println("berkas :" + namaFile + " (tersedia)");
        } else{
            System.out.println("Berkas :" + namaFile + " (sudah dihapus)");
        }

    }
}
