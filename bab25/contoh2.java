import java.util.*;
import java.io.*;

public class contoh2 {
    public static void main2(String[] args){
        Thread t2 = new Thread(new  Mobilku("M-2",430));
        Thread t1 = new Thread(new   Mobilku("M-1",960) );
//-------------------
        t1.start();
        t2.start();
//-----------------
        
    }

    public static void main(String[] args) throws IOException{
        String filename = "config.cfg";
        Properties config = new Properties();

        File fileConfig = new File(filename);
        if( !fileConfig.exists() ){
            System.out.println("tidak tersedia");
        }else{
            System.out.println("file tersedia");
            try{
                FileInputStream streamFile = new FileInputStream( filename );
                config.load( streamFile );
            }catch(IOException io){
                System.err.println("File tidak dikenali");
                System.exit(1);
            }

        }

        System.out.println("root :" + config.getProperty("server", "?"));
    //-------------------------
        System.out.println("isi semula :");
        infoTable( config );
        
    }
    
    static void infoTable(Properties table){
        if( table.isEmpty() ){
            System.out.println("Kamus kosong");
            return ;
        }

        Enumeration kunci = table.propertyNames();
        while( kunci.hasMoreElements() ){
            String id = (String) kunci.nextElement();
            System.out.println(id + " : " + table.getProperty(id) );
        }
        return ;
    }
}

class Mobilku implements Runnable{
    String nama;
    int sleepTime;
    public Mobilku(String name, int sleepTime){
        this.nama = name;
        this.sleepTime = sleepTime;
    }

    public void run(){
        for (char ch = 'A'; ch <= 'J'; ch++) {
            try{
                Thread.currentThread().sleep(this.sleepTime);
            }
            catch(InterruptedException ie){
                System.out.println("Terinterupsi");
            }
            System.out.print("thread: " + nama +"| type: ");
            System.out.println(ch);
        }
    }
}
