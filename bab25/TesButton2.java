import java.awt.*;
public class TesButton2 extends Frame{
    int lebar = 300;
    int panjang = 100;

    public static void main(String[] args){
        TesButton2 apl = new TesButton2();
    }

    public TesButton2(){
        super("Tes Button");
        setSize(lebar, panjang);

        Panel panelTombol = new Panel();
        panelTombol.add( new Button("Perbesar"));
        panelTombol.add( new Button("Keluar"));
        add("South", panelTombol);
        setVisible(true);

    }

    public boolean handleEvent(Event kejadian){
        switch(kejadian.id){
            case Event.WINDOW_DESTROY:
                System.exit(0);
                return true;                
            //    break;
            case Event.ACTION_EVENT:
                System.out.println(kejadian.arg);
                if(kejadian.target instanceof Button){
                    if("Perbesar".equals(kejadian.arg)){
                        lebar += 20;
                        panjang += 15;
                        setSize(lebar, panjang);
                    }
                    else{
                        if("Keluar".equals(kejadian.arg)){
                            System.exit(0);
                            return true;
                        }
                    }
                }
            break;

        }

        return false;
    }
}
