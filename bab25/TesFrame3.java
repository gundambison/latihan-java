import java.awt.*;
import java.awt.event.*;

public class TesFrame3 extends Frame {
    public static void main(String[] args){
        TesFrame3 apl = new TesFrame3();
    }
    //----
    public TesFrame3(){
        super("Frame 3");
        setSize(400, 300);

        addWindowListener(new TesFrame3.PenangananKejadian());

        setVisible(true);
    }

    class PenangananKejadian extends WindowAdapter{
        public void windowClosing(WindowEvent e){
            System.exit(0);
        }
//---------------
        public void windowOpened(){
            System.out.println("buka form");
        }
        public void windowActivated(){
            System.out.println("form active");
        }
        public void windowDeactivated(){
            System.out.println("form deactive");
        }
        public void windowStateChanged(){
            System.out.println("Form State");
        }
    }

}
