import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;

public class TestFrame2 {
    public static void main(String[] args) {
        // Create a new frame
        Frame frame = new Frame("My Frame");

        // Set the size of the frame
        //frame.setSize(400, 300);
        //frame.setExtendedState(Frame.MAXIMIZED_BOTH);
        //MAXIMIZED_BOTH
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        // Set the frame size to the screen size
        frame.setSize(screenSize.width, (screenSize.height / 3));
        
        // Set the position of the frame
        frame.setLocationRelativeTo(null);

        // Make the frame visible
        frame.setVisible(true);
    }
}