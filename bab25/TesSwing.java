import javax.swing.*;

public class TesSwing {
   public static void main(String[] args) {
      JFrame frame = new JFrame("My Swing App");
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      
      JButton button = new JButton("Click me!");
      button.setBounds(50, 50, 100, 30);
      
      frame.add(button);
      frame.setSize(200, 150);
      frame.setLayout(null);
      frame.setVisible(true);
   }
}