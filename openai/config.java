import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class config { 
    public static void main(String[] args){
        String config;
        config = myConfig("apikey");
        System.out.println("config apikey:" + config);
        
        config = myConfig("other");
        System.out.println("config other:" + config);
        
    }

    private static String myConfig(String configName){
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("config.txt"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return prop.getProperty(configName);
    }
}
