import java.util.Vector;
import java.util.Enumeration;
public class Vektor1 {
    public static void main(String[] args){
        Vector kota = new Vector();
        kota.addElement("Bandung");
        kota.addElement("Cirebon");
        kota.addElement("Denpasar");
        kota.addElement("Bogor");

        Enumeration e = kota.elements();
        while(e.hasMoreElements()){
            System.out.println( e.nextElement() );
        }
    }
}
