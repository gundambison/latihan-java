import java.util.BitSet;
public class OpBitset {
    public static void main(String[] args){
        int ukuran = 8;
        BitSet b1 = new BitSet(ukuran);
        //------  
        for(int i=0; i < ukuran; i++){
            b1.set(i);
        }
//------ copy data
        BitSet b2 = (BitSet) b1.clone();
//------ hapus data
        b2.clear(2);
        b2.clear(3);
        b2.clear(6);
//------ 
        System.out.println("b1 :" + b1);
        System.out.println("b2 :" + b2);
//------
        BitSet b3;
        b3 = (BitSet) b1.clone();
        System.out.println("b3 (sebelum   ) :" + b3);
        b3.or(b2);
        System.out.println("b3 (setelah or) :" + b3);
//------
        b3 = (BitSet) b1.clone();
        System.out.println("b3 (sebelum    ) :" + b3);
        b3.and(b2);
        System.out.println("b3 (setelah and) :" + b3);
//------
        b3 = (BitSet) b1.clone();
        System.out.println("b3 (sebelum    ) :" + b3);
        b3.xor(b2);
        System.out.println("b3 (setelah xor) :" + b3);

    }

}