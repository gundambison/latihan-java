import java.util.Hashtable;
import java.util.Enumeration;

public class HashTest { 
    public static void main(String[] args){
        Hashtable tabel = new Hashtable();

        //---input data
        tabel.put("negara", "Indonesia");
        tabel.put("ibukota", "DKI Jakarta");
        tabel.put("matauang", "Rupiah");
        tabel.put("Lagu kebangsaan", "Indonesia Raya");
        tabel.put("Benua", "Asia");

    //------
        System.out.println("Jumlah kunci: "+ tabel.size());
        System.out.println("Isi Semula:");
        infoTable(tabel);

        tabel.put("negara", "Negara Indonesia");
        tabel.remove("Lagu kebangsaan");
        System.out.println("Isi Sekarang:");
        infoTable(tabel);
 
    }

    static void infoTable(Hashtable tabel){
        Enumeration kunci = tabel.keys();
        while(kunci.hasMoreElements()){
            String id = (String) kunci.nextElement();
            System.out.println("id:" +id+"| " + tabel.get(id ));


        }
        
        System.out.println();
    }
}
