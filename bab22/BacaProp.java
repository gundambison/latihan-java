import java.util.*;
import java.io.*;
/*
 * load filenya jadi aneh?
 * 
 */
public class BacaProp {
    public static void main(String[] args) throws IOException{
        String namaBerkas = "Kamus.txt";
        Properties kamus = new Properties();

        File fileKamus = new File(namaBerkas);
        if( !fileKamus.exists() ){
            System.out.println("tidak tersedia");
        }else{
            System.out.println("file tersedia");
            try{
                FileInputStream streamKamus = new FileInputStream( namaBerkas );
                kamus.load( streamKamus );
            }catch(IOException io){
                System.err.println("File tidak dikenali");
                System.exit(1);
            }
        }
    //-------------------------
        System.out.println("isi semula :");
        infoTable( kamus );

        kamus.put("Snake", "Ular"); 
        kamus.put("Horse", "Kuda"); 
        kamus.put("Pigeon", "Burung Dara");
        
        System.out.println();
        System.out.println("isi sekarang :");
        infoTable( kamus );
        //====hapus file
 /*** 
    //---cek
        if(fileKamus.exists()){
            System.out.println("berkas :" + namaBerkas + " (tersedia dan akan dihapus)");
        } else{
            System.out.println("Berkas :" + namaBerkas + " (sudah dihapus)");
        }

        fileKamus.delete();

        //====save data
        FileOutputStream streamKamusSave = new FileOutputStream(namaBerkas);
        DataOutputStream output = new DataOutputStream(streamKamusSave);
    //-----------------
        Enumeration kunci = kamus.propertyNames();
        while( kunci.hasMoreElements() ){
            String id = (String) kunci.nextElement();
            System.out.println("write:"+ id + " = " + kamus.getProperty(id) );
            output.writeUTF(id +" = " + kamus.getProperty(id));
        }
        streamKamusSave.close();
***/
    }

    static void infoTable(Properties table){
        if( table.isEmpty() ){
            System.out.println("Kamus kosong");
            return ;
        }

        Enumeration kunci = table.propertyNames();
        while( kunci.hasMoreElements() ){
            String id = (String) kunci.nextElement();
            System.out.println(id + " : " + table.getProperty(id) );
        }
        return ;
    }
}
