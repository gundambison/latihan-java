import java.util.Vector;
public class Vektor2 {
    public static void main(String[] args){
        Vector kota = new Vector();
        kota.addElement("Bogor");
        kota.addElement("Aceh");
        kota.addElement("Semarang");
        kota.addElement("Cikampek");
        kota.addElement("Palangkaraya");

        tampilVektor("addElement", kota);

        kota.insertElementAt("Kudus", 1);
        tampilVektor("insertElementAt", kota);

        kota.removeElement("Semarang");
        tampilVektor("removeElement: Semarang", kota);

        kota.removeAllElements();
        tampilVektor("removeAllElements", kota);

    }

    static void tampilVektor(String info, Vector v){
        System.out.println(info);
        System.out.println("--------------------");

        if(v.isEmpty()){
            System.out.println("Vektor kosong");
        }else{
            System.out.println("Isi Vektor:");

            for(int i=0; i < v.size(); i++){
                System.out.println(v.elementAt(i));
            }
            System.out.println("--------------------");
        }
    }
}
