import java.util.BitSet;

public class ObjBitset{
    public static void main(String[] args){
        int ukuran = 8;
        
        BitSet infoBit = new BitSet(ukuran);

        for(int i=0; i < ukuran; i++){
            infoBit.set(i);
        }

        //hapus
        infoBit.clear(2);
        infoBit.clear(3);
        infoBit.clear(6);

        //----isi----
        System.out.println("info bit:" + infoBit);

        System.out.println("====");
        System.out.print("InfoBit = ");
        for(int i=0; i < ukuran; i++){
            System.out.print(infoBit.get(i) + " ");
        }

        System.out.println();
        System.out.println("-------");
        
    }
}