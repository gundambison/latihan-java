import java.util.Random;
class RandomTes{
    public static void main(String[] args){
        byte acak[] = new byte[7];

        Random rnd1 = new Random();
        rnd1.nextBytes(acak);

        System.out.println("Random menurut waktu:");

        for(int i=0; i < acak.length; i++){
            System.out.println( (i + 1) + ". " +
                acak[i]);
        }

        //--------
        Random rnd2 = new Random(35);
        rnd2.nextBytes(acak);

        System.out.println("Random dengan Seed");
        for(int i=0; i < acak.length; i++){
            System.out.println( (i + 1) + ". " +
            acak[i]);
        }

    }
}