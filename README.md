# Keterangan Isi Latihan

Hanya untuk menampilkan semua latihan Java yang dilakukan. Sumber pelajaran: **Dasar Pemrograman 
Java 2**.  Latihan ini tidak termasuk applet yang tidak disarankan untuk digunakan

Class tidak termasuk dalam Projek ini. Silahkan generate class sesuai kebutuhan.

untuk compile menjadi class, gunakan

**javac** _nama-file.java_

<ol>
<li>Bab 2. Basic Java minus code buat applet (tidak disarankan)</li>
<li>Bab 3. Dasar Bahasa Java.<ul>
    <li>Konstanta</li>
	<li>Variabel</li>
	<li>Echo / mengeluarkan tampilan untuk tipe angka (bulat) dan String</li>
</ul></li>
<li>Bab 4. Operator dan Ekspresi<ul>
	<li>Operator Bit</li>
	<li>Operator plus. Penggabungan 2 string</li>
	<li>Aritmatika<li>Increment/decrement
</ul></li>
<li>Bab 5. Operator masukan dan keluaran.<ul>
	<li>Membaca inputan
	<li>Memproses Inputan
	<li>Pembuatan Class Baca Keyboard yang akan digunakan bab lainnya
</ul></li>
<li>Bab 6. Ekspresi Kondisi dan pernyataaan.<ul>
	<li>Kondisi
	<li>Switch
</ul></li>
<li>Bab 7. Perulangan<ul>
	<li>For
	<li>While
	<li>Do While
	<li>Break dalam perulangan
</ul></li>
<li>Bab 8. Array Sederhana<ul>
	<li>Array 1 dimensi
	<li>Array 2 dimensi
	<li>Menampilkan bentuk segitiga
</ul></li>
<li>Bab 9. Kelas dan Objek. Penamaan kelas harus berbeda untuk mencegah menimpa nama kelas yang sudah ada<ul>
	<li>penggunaan kelas sederhana
	<li>penggunaan object date dan string
</ul></li>
<li>Bab 10. Konstruktor dan Overloading. Membuat variabel awal pada object dan bentuk input awal dari object bisa memiliki banya variasi</li>
<li>Bab 11. Pewarisan <ul>
	<li>membuat pewarisan
	<li>penggunaan parent/superkelas
	<li>penggunaan protected
	<li>Abstract
	<li>Saling terhubung dengan class berbeda-beda
</ul></li>
<li>Bab 12. Paket dan Import</li>
<li>Bab 13. Interface</li>
<li>Bab 14. Eksepsi<ul>
	<li>Catch
	<li>Catch bertingkat
</ul></li>
<li>Bab 15. Stream</li>
<li>Bab 16. I/O lainnya</li>
<li>Bab 17. Operasi Text</li>
<li>Bab 18. Kelas File dan terkait.<ul>
	<li>kelas File
	<li>Random Access File
</ul></li>
<li>Bab 19. Operasi String</li>
<li>Bab 20. Operasi Matematika (on progress)</li>
<li>Bab 21. Penggunaan Waktu. Disarankan memanfaatkan GregorianCalendar</li>
<li>Bab 22. Utilitas (on progress) </li>
<li>Bab 23. Kelas Lainnya (on progress karena banyak pengulangan penulisan)</li>
<li>Bab 24. Multithreading. Proses terkait antrian yang perlu banyak di asah</li>
<li>Bab 25. Pemrograman Window (On progress)</li>
</ol>

Tambahan<ol>
<li>Open Ai. tidak bisa dijalankan karena perlu bayar untuk dapat menggunakan. tes dilakukan dengan node js</li>
<li>Hrd. Dalam pertimbangan</li>
</ol>
Source: ditulis ulang dari buku Java 2 - Abdul Kadir - 2004