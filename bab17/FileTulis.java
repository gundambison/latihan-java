import java.io.*;

public class FileTulis {
    public static void main(String[] args){

        FileReader berkasMasuk  = null;
        FileWriter berkasKeluar = null;
//--------------
        String fileName = "FileTulis.java";
        String fileNameOut = "fileText.data";
//----------------------
        try{
            berkasMasuk = new FileReader(fileName);
        }catch(IOException io){
            System.err.println("Berkas masukan tidak dapat dibuka");
            System.exit(1);
        }
//-----------
        BufferedReader streamMasukan = new BufferedReader(berkasMasuk);
        //----
        try{
            berkasKeluar = new FileWriter(fileNameOut);
        }catch(IOException io){
            System.err.println("Berkas keluaran tidak dapat dibuka");
            System.exit(2);
        }

        BufferedWriter streamKeluaran = new BufferedWriter(berkasKeluar);
//----------
        try{
            while(true){
                String barisData = streamMasukan.readLine();
                if( barisData == null) break;
            //ubah jadi kapital
                barisData = barisData.toUpperCase( );
            //rekam
                streamKeluaran.write(barisData, 0, barisData.length());
                streamKeluaran.newLine();
            }

        }catch(IOException io){
            System.err.println("Gagal membaca data");
            System.exit(2);
        }
//----------------------
        try{
            streamKeluaran.flush();
        }catch(IOException io){
        }
    //---tutup-----
        try{
            berkasMasuk.close();
            berkasKeluar.close();
        }catch(IOException io){ 
        }
    //---------------
        System.out.println("Hasil ada pada "+ fileNameOut);
        
    }    
}
