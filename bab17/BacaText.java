import java.io.*;
//compile sebelum digunakan
public class BacaText {
    public static void main(String[] args){
    //   int jumByte = 0;
    //    byte bufferData[] = new byte[1024];
    //----
        if(args.length != 1){
            System.err.println("Penggunaan java BacaTeks nama file");
            System.err.println("contoh: java BacaTeks demo.txt");

        }

        String namaBerkas = args[0];
        FileReader berkasMasukan = null;

        try{
            berkasMasukan = new FileReader(namaBerkas);
        }catch( IOException io){
            System.err.println("file tidak dapat dibuka");
            System.exit(2);
        }

        BufferedReader streamMasukan = new BufferedReader(berkasMasukan);

        try{
            while(true){
                String barisData = streamMasukan.readLine();
                if( barisData == null ) 
                    break;

                System.out.println(barisData);

            }
        }catch( IOException io){
            System.err.println("Gagal membaca data");
            System.exit(3);
        }

        try{
            berkasMasukan.close();
        }catch( IOException io){

        }

        System.out.println("===Selesai==");

    }    
}
