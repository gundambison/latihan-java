import java.io.*;

public class FileTulis2 {
    public static void main(String[] args){
        FileReader berkasMasukan = null;
        FileWriter berkasKeluaran = null;
        //====
        String fileInput = "FileTulis2.java";
        String fileOut = "HasilTulis2.data";
        //----
        try{
            berkasMasukan = new FileReader(fileInput);

        }catch(IOException io){
            System.err.println("Berkas masukan tidak dapat dibuka");
            System.exit(1);
        }

        BufferedReader streamMasukan = new BufferedReader(berkasMasukan);
    //------ Berkas keluar
        try{
            berkasKeluaran = new FileWriter(fileOut); 
        }catch(IOException io){
            System.err.println("Berkas Keluaran tidak dapat dibuka");
            System.exit(2);
        }

        BufferedWriter streamKeluaran = new BufferedWriter(berkasKeluaran);
    //------persiapan menulis
        PrintWriter output = new PrintWriter(streamKeluaran);
    //---------------------
    //=== baca dan tulis ke keluaran
    //---------------------
        try{
            while(true){
                String barisData = streamMasukan.readLine();
                if(barisData == null) break;
        //------menjadi kapital
                barisData = barisData.toUpperCase(); 
            //---rekam
                output.println(barisData);
            }

        }catch(IOException io){
            System.err.println("Gagal membaca Data");
            System.exit(3);
        }
//-----tampilkan
        try{
            streamKeluaran.flush();
        }catch(IOException io){            
        }
//------menutup
        try{ 
            berkasKeluaran.close();
            berkasMasukan.close();
        }catch(IOException io){
            System.err.println("Gagal menutup");
            System.exit(4);
        }
    }    
}
//===== akhir file ====