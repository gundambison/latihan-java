public class Konstanta {
    public static void main(String[] args){
        final double PI = 3.14;
        double radius = 30;

        System.out.println("Keliling:\t" + 2 * PI * radius);
        System.out.println('\u3b9F');
    }
}
