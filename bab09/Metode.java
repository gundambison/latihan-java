class Lingkaran{
//variable
    private double radius ;
    void isiJariJari(double radius){
        this.radius = radius;
    }

    private double perolehPi(){
        return 3.4141;
    }

    public double perolehKeliling(){
        return 2 * perolehPi() * radius;
    }

}

public class Metode {
    public static void main(String[] args){
        Lingkaran bulatan = new Lingkaran();
        //isi nilai jari-jari
        bulatan.isiJariJari(75);
        //tampilkan
        System.out.println("Keliling =" + bulatan.perolehKeliling());
        //tidak diperbolehkan
        //System.out.println("Keliling =" + bulatan.perolehPi());
        //nilai akar 25
        System.out.println("Akar 25 = " + Math.sqrt(25));
    }
}
