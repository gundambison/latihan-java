class Mobilku{
    String nama;
    int tahun;
    String warna;
    void isiData(String MobilNama, String MobilWarna, int MobilTahun){
        nama = MobilNama;
        warna = MobilWarna;
        tahun = MobilTahun;
    }

    String namaMobil(){
        return nama;
    }
    int tahunMobil(){
        return tahun;
    }
}

public class KelasMobil2 {
    public static void main( String[] args) {
        Mobilku cMobil = new Mobilku();
        cMobil.isiData("Mazda", "Biru", 2006);
        // cMobil.nama = "Mazda";
        // cMobil.warna = "Merah";
        // cMobil.tahun = 1999;
        
        //System.out.println("nama mobil: "+ cMobil.nama + " berwarna " + cMobil.warna);
        //System.out.println("tahun pembuatan mobil: "+ cMobil.tahun);
        System.out.println("nama mobil: "+ cMobil.namaMobil());
        System.out.println("Tahun mobil: "+ cMobil.tahunMobil());
    }
}
