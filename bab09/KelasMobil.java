class Mobil{
    String nama;
    int tahun;
    String warna;
}
public class KelasMobil {
    public static void main( String[] args) {
        Mobil cMobil = new Mobil();

        cMobil.nama = "Mazda";
        cMobil.warna = "Merah";
        cMobil.tahun = 1999;

        System.out.println("nama mobil: "+ cMobil.nama + " berwarna " + cMobil.warna);
        System.out.println("tahun pembuatan mobil: "+ cMobil.tahun);

    }
}
