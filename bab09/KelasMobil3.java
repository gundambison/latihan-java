class Mobil3{
    String nama;
    int tahun;
    String warna;
    void isiData(String MobilNama, String MobilWarna, int MobilTahun){
        this.nama = MobilNama;
        this.warna = MobilWarna;
        this.tahun = MobilTahun;
    }

    String namaMobil(){
        return this.nama;
    }
    int tahunMobil(){
        return this.tahun;
    }
}

public class KelasMobil3{
    public static void main( String[] args) {
        Mobil3 cMobil = new Mobil3();
        cMobil.isiData("Mazda", "Biru", 2006);
        // cMobil.nama = "Mazda";
        // cMobil.warna = "Merah";
        // cMobil.tahun = 1999;
        
        //System.out.println("nama mobil: "+ cMobil.nama + " berwarna " + cMobil.warna);
        //System.out.println("tahun pembuatan mobil: "+ cMobil.tahun);
        System.out.println("Nama mobil : "+ cMobil.namaMobil());
        System.out.println("Tahun mobil: "+ cMobil.tahunMobil());
    }
}
