import java.util.Arrays;
import java.util.Comparator;

public class SortDecending {
    public static void main(String[] args){
        String kota[] = {
            "Kudus", "Semarang", "Bandung",
            "Makasar", "Ciganjur", "Jakarta",
            "Tangerang", "Yogyakarta", "Bogor",
            
        };

        System.out.println("Sebelum di urutkan");
        showArray(kota);

        Arrays.sort(kota, new UrutTurun());
        System.out.println();
        System.out.println("Sesudah di urutkan");
        showArray(kota);
    }
    
    public static void showArray(String[] arr){
        int i2;
        for(int i=0; i < arr.length ; i++){
            i2 = i + 1;
            System.out.println(i2 +". "+arr[i] );

        }
        return ;
    }
}

class UrutTurun implements Comparator{
    public int compare(Object o1, Object o2){
        String s1 = (String) o1;
        String s2 = (String) o2;
    //---------------------------
        int hasil = s1.compareTo(s2);
        hasil = -hasil;
        return hasil;
    }
}