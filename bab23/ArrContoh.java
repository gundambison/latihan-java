import java.util.Arrays;
import java.util.ArrayList;

public class ArrContoh {
    public static void main(String[] args){
        String kotaBase[] = {
            "Kudus", "Semarang", "Bandung",
            "Makasar", "Ciganjur", 
        };

        ArrayList kota = new ArrayList();

        kota.add("Padang");
        for(int i=0; i < kotaBase.length ; i++){
            kota.add(kotaBase[i]);

        }

        System.out.println("Jumlah data: " + kota.size() );
        
        System.out.println();
        System.out.println("Isi sebelum");
        hasilArrayList(kota);
        
        kota.remove(5);
        System.out.println();
        System.out.println("Isi sesudah");
        hasilArrayList(kota);

        System.out.println();
        System.out.println("Isi kosong");
        kota.clear();
        hasilArrayList(kota);

    }

    public static void hasilArrayList(ArrayList row){
        if(!row.isEmpty()){
            for(int i=0; i < row.size();i++){
                System.out.println(row.get(i));
            }
        }else{
            System.out.println("Tidak ada Isinya");
        }

        return ;

    }
}
