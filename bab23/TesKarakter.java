public class TesKarakter {
    public static void main(String[] args){
        String st = "Tes  .  12\n3 ";

        for(int i=0; i < st.length(); i++){
            char kar = st.charAt( i );
            if(Character.isDigit(kar)){
                System.out.println(kar + ": Digit");
            }else if(Character.isLowerCase(kar)){
                System.out.println(kar + ": Lower");
            }else if(Character.isUpperCase(kar)){
                System.out.println(kar + ": Upper");
            }else if(Character.isWhitespace(kar)){
                System.out.println(kar + ": spasi putih");
            }else{
                System.out.println(kar + ": Lain-lain");
            }
        }
        
        System.out.println("=========================");

    }

}

