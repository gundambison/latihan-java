import java.util.Arrays;

public class SortArr{
    public static void main(String[] args){
        String kota[] = {"Kudus", "Semarang", "Bandung",
    "Solo", "Ciganjur", "Jakarta",
    "Tangerang", "Aceh", "Bogor"};

        System.out.println("data semula:");
        for(int i=0; i < kota.length ; i++){
            System.out.println(i +". "+kota[i] );

        }

        Arrays.sort(kota);
        System.out.println();
        System.out.println("Setelah di urutkan");
        for(int i=0; i < kota.length ; i++){
            System.out.println(i +". "+kota[i] );

        }
        
        System.out.println();
    }
}