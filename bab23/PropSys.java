import java.util.Properties;
import java.util.Enumeration;

public class PropSys {
    public static void main(String[] args){
        Properties p = System.getProperties();

        Enumeration elemen = p.propertyNames();
        while(elemen.hasMoreElements()){
            String namaProperty = (String) elemen.nextElement();
            System.out.println( namaProperty + " = " + p.getProperty(namaProperty) );

        }

        String namaOS = p.getProperty("os.name");
        System.out.println("OS : " + namaOS);
    }
}
