public class TesInteger {
    public static void main(String[] args){
        int bilangan = 1214;
        //----
        System.out.println("Sistem Desimal : " + bilangan );
        System.out.println("Sistem hexadesimal : " + Integer.toHexString(bilangan)  );
        System.out.println("Sistem Oktal       : " + Integer.toOctalString(bilangan) );
        System.out.println("Sistem biner       : " + Integer.toBinaryString(bilangan) );
        //-----
        System.out.println("Sistem basis 12 : " + Integer.toString(bilangan,12 ) );
        
    }
    
}
