class Sepeda extends Kendaraan{
    public Sepeda(String nama){
        this.nama = nama;
    }

    public void jalankan(){
        System.out.println("Duduk di Sadel dan kayuh \""+
        this.nama +"\"");
    }
}

class Minibus extends Kendaraan{
    public Minibus(String nama){
        this.nama = nama;
    }

    public void jalankan(){
        System.out.println("Duduk di depan setir '"+
        this.nama + "' dan nyalakan mesin");
    }
}

//------main----
public class ProjAbstract {
    public static void main(String[] args){
        Sepeda objSepeda = new Sepeda("Harrington");
        Minibus objMini = new Minibus("Ryan Blue");
    //======show
        objSepeda.jalankan();
        objMini.jalankan();
    //----------------
    }
//====================
    
}
