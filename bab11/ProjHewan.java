//import java.security.PublicKey;//????

class Mahluk{
    public void info(){
        System.out.println("Di jalankan di Mahluk");
    }
}

class Mamalia{
    public void info(){
        System.out.println("Di jalankan di Mamalia");
    }
}

class Sapi{
    public void info(){
        System.out.println("Di jalankan di Sapi");
    }
}

//----Polimor
class ProjHewan {
    public static void main(String[] args){
        Mamalia objMamalia;
        Mahluk objMahluk;
        Sapi objSapi;

        objMahluk = new Mahluk();
        objMahluk.info();

        objMamalia = new Mamalia();
        objSapi = new Sapi();
        //----
        objMamalia.info();
        objSapi.info();
    }
}
