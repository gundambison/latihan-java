class Garis {
    private Koordinat posisi1;
    private Koordinat posisi2;

    public Garis(int x1, int y1, int x2, int y2){
        this.posisi1 = new Koordinat(x1, y1);
        this.posisi2 = new Koordinat(x2, y2);

    }

    public void info(){
        System.out.println("Koordinat pertama");
        System.out.println( posisi1.ambilX() + ", " + posisi1.ambilY() );
        System.out.println();
        System.out.println("Koordinat Kedua");
        System.out.println( posisi2.ambilX() + ", " + posisi2.ambilY() );
        System.out.println();
    }

}
