class Orang {
    private String nama;
    private int usia;

    public Orang(String nama, int usia){
        this.nama = nama;
        this.usia = usia;

    }

    public void info(){
        System.out.println("Nama : " + this.nama);
        System.out.println("Usia : " + this.usia);

    }

}
//---pegawai---
class Pegawai extends Orang{
    protected String noPegawai;

    public Pegawai(String noPegawai, String nama, int usia){
        super(nama, usia);
        this.noPegawai = noPegawai;
    }

    public void info(){
        super.info();
        System.out.println("No Pegawai: " + this.noPegawai );
        
    }
}

public class ParentOrang{
    public static void main(String[] args){
        Pegawai programer = new Pegawai("3142151", "Gunawan Wibi", 42);
        programer.info();
    }
    
}