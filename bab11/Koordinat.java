//compile ini
class Koordinat {
    private int x;
    private int y;
    public Koordinat(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void ubahX(int x){
        this.x = x;
    }

    public void ubahY(int y){
        this.y = y;
    }

    public int ambilX(){
        return this.x;
    }

    public int ambilY(){
        return this.y;
    }
}
