//memakai Final
class Angka {
    final public double PI = 3.14;
    final void info(){
        System.out.println("memakai Final. " +  "Dipanggil pada " + this.getClass().getName() );
        System.out.println("PI = " + PI );
    }

}

class Math01 extends Angka{
    public void info2(){ // tidak akan bisa kalau info
        //something write here
    }
}

class Final{
    public static void main(String[] args){
        Math01 obj = new Math01();
        obj.info();
    }
}