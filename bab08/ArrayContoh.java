public class ArrayContoh{
    public static void main(String[] args){
        String[] kota;
        kota = new String[5];
       //---------------------- 
        kota[0] = "Jakarta";
        kota[1] = "Bandung";
        kota[2] = "Bogor";
        kota[3] = "Medan";
        kota[4] = "Yogya";
       //---------------------- 
       System.out.println("kota " + kota[0]);
       System.out.println("kota " + kota[1]);
       System.out.println("kota " + kota[2]);

       for(int i=0; i<5; i++){
            System.out.println("kota "+i+":" + kota[i]);
       }
    }
}