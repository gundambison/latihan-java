import java.io.*;

public class Contoh{
    public static void main(String[] args){
        System.out.println("Nilai E : " + Math.E);
        System.out.println("Nilai Pi : " + Math.PI);
        //-----
        System.out.println("ln 20 = " + Math.log(20.0));
        System.out.println("Ceil (-5.6) = " + Math.ceil(-5.6) );
        System.out.println("Floor (-5.6) = " + Math.floor(-5.6) );
        System.out.println("Round (-5.6) = " + Math.round(-5.6) );
        //-----
        System.out.println("sqrt (25) = " + Math.sqrt(25) );
        System.out.println("pow (25, 0.5) = " + Math.pow(25, 0.5) );
        //----
        double num;
        num = Math.log(20.0);
       System.out.println("ln 20 = " + Math.exp(num));

        
    }
}