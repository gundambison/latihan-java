import java.io.*;
public class Lintasan {
    public static void main(String[] args) throws IOException{
        BufferedReader dataBaris = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Menghitung jarak Peluru");
        System.out.print("Sudut tembakan: ");
        String st = dataBaris.readLine();
        double derajat = Double.valueOf(st).intValue();
        double radian  = derajat * Math.PI / 180;

        System.out.print("Kecepatan tembakan: ");
        st = dataBaris.readLine();
        double kecepatan = Double.valueOf(st).doubleValue();

        double jarak = 2 * Math.pow( kecepatan, 2) * Math.sin(radian) *
            Math.cos(radian) / 9.8 ;

        System.out.println("peluru jatuh pada : " + jarak );

    }
}
