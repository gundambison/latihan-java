public class Contoh{
    public static void main(String[] args){
        byte data[] = new byte[6];
    //--- input data
        data[0] = 64;
        data[1] = 79;
        data[2] = 69;
        data[3] = 83;
        data[4] = 85;
        data[5] = 77;
    //--- String
        String s1 = "Selamat Pagi";
        String s2 = new String("Good Morning");
        String s3 = new String(data);
        String s4 = new String(data, 2, 3);

        System.out.println("S1 hasil: "+ s1);
        System.out.println("S2 hasil: "+ s2);
        System.out.println("S3 hasil: "+ s3);
        System.out.println("S4 hasil: "+ s4);
    }
}