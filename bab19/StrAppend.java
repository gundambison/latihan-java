public class StrAppend {
    public static void main(String[] ars){
        StringBuffer st = new StringBuffer("Baris 1 \n") ;
        //I would like to announce the arrival of the Tactical Battle System plugin  ";
        System.out.println("isi       : " + st.toString() );
        //--append
        st.append(true);
        st.append('\n');

        st.append( (double) 76.89243 );
        st.append('\n');
        
        st.append( (float) 75.89243 );
        st.append('\n');
//------info
        System.out.println("Penambahan Append==========");
        System.out.println("isi       : " + st.toString() );
        System.out.println("kapasitas : " + st.capacity() );
        System.out.println("panjang   : " + st.length()   );
        
    }
}
