public class StrBuff {
    public static void main(String[] ars){
        StringBuffer st = new StringBuffer(25) ;
        //I would like to announce the arrival of the Tactical Battle System plugin  ";

        System.out.println("isi       : " + st.toString() );
        System.out.println("kapasitas : " + st.capacity() );
        System.out.println("panjang   : " + st.length()   );
        
        //--append
        st.append("Selamat ");
        st.append("Belajar JAVA ");

        System.out.println("Penambahan Append==========");
        System.out.println("isi       : " + st.toString() );
        System.out.println("kapasitas : " + st.capacity() );
        System.out.println("panjang   : " + st.length()   );

        //-----
        st.setLength(7);
        System.out.println("Perubahan panjang==========");
        System.out.println("isi       : " + st.toString() );
        System.out.println("kapasitas : " + st.capacity() );
        System.out.println("panjang   : " + st.length()   );
        
    }
}
