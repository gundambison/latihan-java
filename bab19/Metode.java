public class Metode {
    public static void main(String[] ars){
        String st = "  Tes, tes, Test, .... 123 ";
        //I would like to announce the arrival of the Tactical Battle System plugin  ";

        System.out.println("lower case: " + st.toLowerCase() );
        System.out.println("upper case: " + st.toUpperCase() );
        System.out.println("not trim: [" + st +"]" );        
        System.out.println("trim    : [" + st.trim() +"]" );

        System.out.println("-------------------" );
        System.out.println("karakter ke 4 : " + st.charAt(4)   );
        System.out.println("posisi ke 12  : " + st.indexOf("12")   );
        System.out.println("substr(5, 8)  : " + st.substring(5, 8)+"|"   );
        
    }
    
}
