public class CompareStr {
    public static void main(String[] args){
        String st = "Yogya";
        int hasil;
        boolean logika;
    //-------
        System.out.println("CompareTo");
        //-----
        hasil = st.compareTo("Yogya");
        System.out.println("hasil (Yogya) :" + hasil);
        //-----
        hasil = st.compareTo("YOGYA");
        System.out.println("hasil (YOGYA) :" + hasil);
        //-----
        hasil = st.compareTo("yogya");
        System.out.println("hasil (yogya) :" + hasil);
    //--------
        System.out.println("Equal dan Equal Ignore Case");
        //-----
        logika = st.equals("yogya");
        System.out.println("hasil        (yogya) :" + logika);
        logika = st.equalsIgnoreCase("yogya");
        System.out.println("hasil ignore (yogya) :" + logika);

    }
}
