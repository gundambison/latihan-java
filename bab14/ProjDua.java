class ProjDua{
    public static void main(String[] args){
        System.out.println("pembagian bermasalah");

        try{
            System.out.println( 5 / 0 );
        }catch(Throwable t){
            System.err.println("Terjadi kesalahan pembagian");
            System.err.println(t.getMessage());
        }

        System.out.println("pembagian Selesai");

        double BILANGAN = 100.0;

        for(int i=6; i >= 0; i--){
            try{
                System.out.print(BILANGAN + " / " + i + " = ");
                System.out.println( BILANGAN / i );
            }finally{
                System.out.println("Bagian FINALLY dijalankan");

            }
            
        }

        System.out.println("Selesai");

    }
}