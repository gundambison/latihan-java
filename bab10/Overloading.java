class Matematika{
    static public double kuadrat( double nilai){
        return nilai * nilai;
    }

    static public int kuadrat(int nilai){
        return nilai * nilai;
    }

    static public double kuadrat(String nilai){
        double bilangan;
        //convert
        bilangan = Double.valueOf(nilai).doubleValue();
        return bilangan * bilangan;

    }
}

public class Overloading {
    public static void main(String[] args){
        System.out.println("Kuadrat (1) : " + Matematika.kuadrat(25.0));
        System.out.println("Kuadrat (2) : " + Matematika.kuadrat(24 ));
        System.out.println("Kuadrat (3) : " + Matematika.kuadrat("20.0"));
    }    
}
