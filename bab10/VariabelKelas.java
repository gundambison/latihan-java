class Bilangan{
    static  int pencacah = 0;
    private int nilai;

    public Bilangan(int nilai){
        this.nilai = nilai;
        this.pencacah++;
    }

    public void info(){
        System.out.println("Nilai    :" + this.nilai );
        System.out.println("Pencacah :" + this.pencacah );
        System.out.println( );
    }
}

public class VariabelKelas {
    public static void main(String[] args){

        Bilangan b1 = new Bilangan(55);
        b1.info();
        
        Bilangan b2 = new Bilangan(75);
        b2.info();
        
        Bilangan b3 = new Bilangan(85);
        b3.info();
    }
}
