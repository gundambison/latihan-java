class Mobil{
//variable Instan
    private String warna;
    private int tahunProduksi;

    public Mobil(String warna, int tahunProduksi){
        this.warna = warna;
        this.tahunProduksi = tahunProduksi;

    }

    public Mobil(){
        //is empty
    }

    public void info(){
        System.out.println("Warna : " + this.warna );
        System.out.println("Tahun : " + this.tahunProduksi );
    }
}

public class Konstruktor2{
    public static void main(String[] args){
        Mobil clMobil = new Mobil("Biru", 1998);
        clMobil.info();
        Mobil clMobil2 = new Mobil();
        clMobil2.info();

    }
    
}