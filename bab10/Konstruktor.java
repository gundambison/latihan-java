class Mobil{
//variable Instan
    private String warna;
    private int tahunProduksi;

    public Mobil(String warna, int tahunProduksi){
        this.warna = warna;
        this.tahunProduksi = tahunProduksi;

    }

    public void info(){
        System.out.println("Warna : " + this.warna );
        System.out.println("Tahun : " + this.tahunProduksi );
    }
}

public class Konstruktor{
    public static void main(String[] args){
        Mobil clMobil = new Mobil("Merah", 1998);
        clMobil.info();

    }
    
}