class Bentuk{
    private int[] koordinat;
    //konstruktor
    public Bentuk(int x1, int y1){
        this.koordinat = new int[2]; 
        this.koordinat[0] = x1;
        this.koordinat[1] = y1;
    }

    public Bentuk(int x1, int y1, int x2, int y2){
        this.koordinat = new int[4]; 
        this.koordinat[0] = x1;
        this.koordinat[1] = y1; 
        this.koordinat[2] = x2;
        this.koordinat[3] = y2;
    }

    public Bentuk(int x1, int y1, 
                  int x2, int y2,
                  int x3, int y3){
        this.koordinat = new int[6]; 
        this.koordinat[0] = x1;
        this.koordinat[1] = y1; 
        this.koordinat[2] = x2;
        this.koordinat[3] = y2;
        this.koordinat[4] = x3;
        this.koordinat[5] = y3;
    }

    public void info(){
        int panjang = this.koordinat.length;
        switch(panjang){
            case 2: 
                System.out.println("Bentuk Titik");
                System.out.println("Koordinat :");
                System.out.println(koordinat[0] + " - " + koordinat[1]);
            break;
            case 4: 
                System.out.println("Bentuk Garis");
                System.out.println("Koordinat :");
                System.out.println(koordinat[0] + " - " + koordinat[1]);
                System.out.println(koordinat[2] + " - " + koordinat[3]);
            break;
            case 6: 
                System.out.println("Bentuk Segitiga");
                System.out.println("Koordinat :");
                System.out.println(koordinat[0] + " - " + koordinat[1]);
                System.out.println(koordinat[2] + " - " + koordinat[3]);
                System.out.println(koordinat[4] + " - " + koordinat[5]);
            break;
            default:
                System.out.println("Bentuk Tak diketahui");
        }
    }
}

public class Konstruktor3 {
    public static void main(String[] args){
        Bentuk bentuk1 = new Bentuk(4, 7);
        bentuk1.info();

        Bentuk bentuk2 = new Bentuk(5, 6, 12, 9);
        bentuk2.info();

        Bentuk bentuk3 = new Bentuk(1, 4, 5, 8, 10, 9);
        bentuk3.info();
    }    
}
