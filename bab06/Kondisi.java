public class Kondisi {
    
    public static void main(String[] args){
        System.out.print("Total Belanja : ");
        String st = BacaKeyboard.bacaString(); // memakai class di bab 5
        int totalBelanja = Integer.valueOf(st ).intValue(); //merubah menjadi integer

        int diskon;
        diskon = 0;
        if(totalBelanja >= 10000000 ){
            diskon = totalBelanja / 10;
        }

        System.out.println("total diskon: "+diskon);
    }
}
