public class KondisiSwitch {
    public static void main(String[] args){
//dibutuhkan BacaKeyboard.class dari bab 5. Atau compile dari script yang ada
        System.out.println("Pilihan :");
        System.out.println("1. Soto Ayam");
        System.out.println("2. Nasi Rawon");
        System.out.println("3. Gulai Kambing");
        System.out.println("==================");

        System.out.print("Pilihan anda (enter jika selesai): ");
        String st = BacaKeyboard.bacaString();
        int pilihan = Integer.valueOf(st).intValue();
//------Switch-----
        switch( pilihan ){
            case 1:
                System.out.println("Pilihan anda Soto Ayam");
                System.out.println("Silahkan Menunggu sebentar");
                break;
            case 2:
                System.out.println("Pilihan anda Nasi Rawon");
                System.out.println("Silahkan Menunggu sebentar");
                break;
            case 3:
                System.out.println("Pilihan anda Gulai Kambing");
                System.out.println("Silahkan Menunggu sebentar");
                break;

            default:
                System.out.println("Silahkan memilih 1, 2, dan 3");
                
        }
        System.out.println("Akhir Script Swich");
    }
}
