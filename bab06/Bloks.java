public class Bloks {
    public static void main(String[] args){
        int x = 10;
        System.out.println("Block Soal");
        System.out.println("nilai sebelum x = "+x);
        {
            int y;
            y = 50;
            x = x + 50;
            System.out.println("Didalam Blok");
            System.out.println("x = " + x);
            System.out.println("y = " + y);
        }
        System.out.println("nilai setelah x = " + x);

        x = 10;
        System.out.println("nilai sebelum x = " + x);
        int y2 =50;
        x += y2;
        System.out.println("nilai sesudah x = " + x);

    }
}
